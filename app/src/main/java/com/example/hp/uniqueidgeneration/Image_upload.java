package com.example.hp.uniqueidgeneration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;


public class Image_upload extends AppCompatActivity implements View.OnClickListener {

    ImageView imgview;
    Button btnnext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        imgview=(ImageView) findViewById(R.id.imageView);
        btnnext=(Button) findViewById(R.id.button2);

        btnnext.setOnClickListener(this);
        imgview.setOnClickListener(this);
    }

    String path="";
    String imageinbase64="";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode== 100)
        {
            if (resultCode == RESULT_OK)
            {


                Uri uri=data.getData();

                try
                {
                    path = FileUtils.getPath(this, uri);
                    imgview.setImageURI(uri);


                }
                catch (URISyntaxException e)
                {

                    e.printStackTrace();
                }

                try
                {
                    File file = new File(path);
                    byte[] b = new byte[819211];

                    byte[] byteArray = null;

                    InputStream inputStream = new FileInputStream(file);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                    int bytesRead =0;

                    while ((bytesRead = inputStream.read(b)) != -1)
                    {
                        bos.write(b, 0, bytesRead);
                    }

                    byteArray = bos.toByteArray();

                    imageinbase64 = Base64.encodeToString(byteArray, Base64.NO_WRAP);

                    SharedPreferences shp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor ed= shp.edit();
                    ed.putString("photo",imageinbase64);
                    ed.commit();








                }
                catch (IOException e)
                {
                    Toast.makeText(this,"String :"+e.getMessage().toString(), Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view==btnnext) {
            Intent obj = new Intent(getApplicationContext(), Scan_QR.class);
            startActivity(obj);
        }
        else{

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT); //getting all types of files
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            try {
                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"),100);
            } catch (android.content.ActivityNotFoundException ex) {
                // Potentially direct the user to the Market with a Dialog
                Toast.makeText(getApplicationContext(), "Please install a File Manager.",Toast.LENGTH_SHORT).show();
            }
        }


    }
}
