package com.example.hp.uniqueidgeneration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends AppCompatActivity implements View.OnClickListener {

    EditText edip;
    Button btset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edip=(EditText) findViewById(R.id.editText);
        btset=(Button) findViewById(R.id.button);


        btset.setOnClickListener(this);






    }

    @Override
    public void onClick(View view) {

        if(view==btset) {

            String ip = edip.getText().toString();


            if (ip.length() == 0) {
                edip.setError("missing");
            } else {


                SharedPreferences shp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor ed = shp.edit();
                ed.putString("ip", ip);
                ed.commit();


//            Toast.makeText(getApplicationContext(),"Hi",Toast.LENGTH_SHORT).show();
                Intent obj = new Intent(getApplicationContext(), Image_upload.class);
                startActivity(obj);
            }

        }


    }
}
